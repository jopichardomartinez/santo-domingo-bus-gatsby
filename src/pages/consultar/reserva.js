import React from "react";
import {
  AiOutlineCalendar,
  AiOutlineClockCircle,
  AiOutlineUser,
} from "react-icons/ai";
import { Layout } from "../../components/layout";

export default function Buscar() {
  return (
    <Layout className="gap-4 flex-col flex p-0">
      <>
        <div className="bg-white px-8 py-4">
          <h2 className="text-xl font-bold">Consulta tu reservación</h2>
          <div className="grid grid-rows-2 md:grid-cols-6 gap-4 md:grid-rows-1 py-2">
            <div className="bg-white p-4 rounded-xl border-2">
              <label htmlFor="to" className="block text-sm font-medium">
                Código de reserva
              </label>
              <input
                type="text"
                name="to"
                className="w-full focus:border-none text-xl"
                placeholder="A1B2C3D4"
              />
            </div>

            <button className="bg-secondary-color rounded-xl p-4 flex justify-center text-white gap-2 items-center">
              <span className="text-xl font-medium">Buscar</span>
            </button>
          </div>
        </div>
        <div className="px-8 bg-slate-100">
          <div className="bg-slate-100 flex flex-col py-8">
            <div className="grid grid-rows-2 md:grid-cols-2 md:grid-rows-1 gap-8">
              <div className="flex flex-col bg-white p-8 gap-8">
                <h2 className="text-2xl font-bold">
                  Información general de viaje
                </h2>

                <div className="flex flex-col gap-2">
                  <span className="block text-secondary-color text-sm font-bold">
                    Salida
                  </span>
                  <h2 className="text-2xl font-bold">Santo Domingo </h2>
                  <p className="text-gray-500">
                    Avenida Las Americas #52, esq. av. Sabana Larga, Santo
                    Domingo
                  </p>
                  <div className="grid grid-cols-3 py-4">
                    <div className="flex gap-4 items-center font-bold">
                      <AiOutlineCalendar size={32}></AiOutlineCalendar>
                      <span className="text-md">dd/mm/aaaa</span>
                    </div>
                    <div className="flex gap-4 items-center font-bold">
                      <AiOutlineClockCircle size={32}></AiOutlineClockCircle>
                      <span className="text-md">hh:mm</span>
                    </div>
                    <div className="flex gap-4 items-center font-bold">
                      <AiOutlineUser size={32}></AiOutlineUser>
                      <span className="text-md">00</span>
                    </div>
                  </div>
                </div>

                <div className="flex flex-col gap-2 pb-4 mb-4 border-b-2">
                  <span className="block text-secondary-color text-sm font-bold">
                    Destino
                  </span>
                  <h2 className="text-2xl font-bold">Romana</h2>
                  <p className="text-gray-500">
                    Calle Noseque, Hotel Nosecuanto, La Romana
                  </p>
                </div>

                <div className="flex flex-col gap-2 pb-4">
                  <h2 className="text-xl font-bold">Comentarios</h2>
                  <p className="text-gray-500">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                    sed diam nonummy.{" "}
                  </p>
                </div>
              </div>

              <div className="grid grid-rows-2">
                <div className="flex flex-col p-8 gap-8 bg-white">
                  <h2 className="text-2xl font-bold">
                    Información general de viaje
                  </h2>
                  <div className="grid grid-rows-2 md:grid-cols-2 md:grid-rows-1">
                    <div>
                      <span className="block text-xs font-light">
                        Nombre y apellido
                      </span>
                      <h2 className="text-2xl">John Doe</h2>
                    </div>
                    <div>
                      <span className="block text-xs font-light">
                        Cédula de identidad/Pasaporte
                      </span>
                      <h2 className="text-2xl">000 0000000 0</h2>
                    </div>
                  </div>
                  <div className="grid grid-rows-2 md:grid-cols-2 md:grid-rows-1">
                    <div>
                      <span className="block text-xs font-light">E-mail</span>
                      <h2 className="text-2xl">johndoe@mail.com</h2>
                    </div>
                    <div>
                      <span className="block text-xs font-light">Télefono</span>
                      <h2 className="text-2xl">+1 000 000 0000</h2>
                    </div>
                  </div>
                </div>
                <div className="flex flex-col p-8 justify-center items-center">
                  <div className="flex flex-col gap-4 text-center">
                    <img
                      src={"https://file.rendit.io/n/UzBKadTsN6KErrxPRrDU.png"}
                      alt=""
                    />
                    <h2 className={"text-3xl font-bold"}>
                      USD${Math.ceil(Math.random() * 100)}.00
                    </h2>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    </Layout>
  );
}
