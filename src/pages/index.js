import * as React from "react";
import { FaBalanceScale, FaWhatsapp } from "react-icons/fa";
import { BsChevronDoubleRight, BsShieldCheck } from "react-icons/bs";
import { BiCog } from "react-icons/bi";
import { TbHeartHandshake } from "react-icons/tb";
import { Image } from "../components/image";
import { ServiceCard } from "../components/index/services";
import { QuickSearchTripForm } from "../components/index/quick-search-trip-form";
import { Layout } from "../components/layout";
import { TestimonialCard } from "../components/index/testimony";
import { CharacteristicCard } from "../components/index/characteristic-card";

// markup
const IndexPage = (props) => {
  const testimonials = [
    {
      id: 0,
      comment:
        "Hemos contratado este empresa varias veces por su buen servicio.  Y el precio es el mejor. Comprobado.",
      name: "Marisol y Ruddy Abreu",
      rating: 5,
      image:
        "https://i.insider.com/5899ffcf6e09a897008b5c04?width=1000&format=jpeg&auto=webp",
    },
    {
      id: 1,
      comment: "Muy buen servicio, muy buen precio",
      name: "Juan y Maria",
      rating: 5,
      image:
        "https://i.insider.com/5899ffcf6e09a897008b5c04?width=1000&format=jpeg&auto=webp",
    },
    {
      id: 2,
      comment:
        "Very nice people. Low price. And excellent service, as promised. The driver was very friendly and respectful.",
      name: "James W.",
      rating: 5,
      image:
        "https://images.unsplash.com/photo-1570295999919-56ceb5ecca61?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8bWFsZSUyMHByb2ZpbGV8ZW58MHx8MHx8&w=1000&q=80",
    },
  ];

  const CharacteristicsSection = () => {
    return (
      <>
        {/* plan your trip section */}
        <Section className="flex flex-col">
          <div className="grid grid-cols-1 gap-8 px-8 md:px-48 text-center">
            <div className="flex flex-col">
              <h6 className="text-green-500 font-montserrat text-xs font-bold uppercase">¿Qué garantizamos?</h6>
              <h3 className="text-3xl font-bold text-primary-color-dark font-montserrat">
                Calidad en cada viaje
              </h3>
            </div>
            <div className="grid gap-4 grid-rows-3 md:grid-cols-3 md:grid-rows-1">
              <CharacteristicCard
                characteristic={{
                  image: "shield_icon.png",
                  title: "Seguridad y confianza",
                  description:
                    "Todas nuestras unidades son constantemente inspeccionadas para brindarte seguridad y confianza.",
                }}
              ></CharacteristicCard>
              <CharacteristicCard
                characteristic={{
                  image: "money_icon.png",
                  title: "Tarifas asequibles",
                  description:
                    "Nos aseguramos de brindar un servicio de calidad y adsequible. Comprueba nuestras bajísimas tarifas!",
                }}
              ></CharacteristicCard>
              <CharacteristicCard
                characteristic={{
                  image: "feather_icon.png",
                  title: "Comfort",
                  description:
                    "Disfruta de la tranquilidad que brinda un servicio confortable desde y hacia todos los aeropuertos de Republica Dominicana..",
                }}
              ></CharacteristicCard>
            </div>
            <div className="justify-center flex">
              <button className="bg-secondary-color rounded-xl p-4  flex justify-center text-white gap-2 items-center">
                <span className="font-medium">Reserva con nosotros</span>
                <BsChevronDoubleRight size={24} />
              </button>
            </div>
          </div>
        </Section>
      </>
    );
  };

  const Section = (props) => {
    return (
      <section
        className={`py-6 px-6 md:py-20 md:px-52 bg-white ${props.className}`}
        style={props.style}
        id={props.id}
      >
        {props.children}
      </section>
    );
  };

  const PlanYourNextTripSection = () => {
    return (
      <>
        {/* characteristics */}
        <Section className="flex flex-col bg-slate-100">
          <div className="grid grid-rows-2 md:grid-cols-2 md:grid-rows-1 gap-4">
            <div className="flex flex-col justify-center gap-4 p-8 md:p-16 font-medium items-stretch">
              <span className="block font-bold text-4xl font-montserrat">
                Planifica
                <span className="block">tu próximo viaje</span>
                <span className="font-medium">con nosotros</span>
              </span>
              <div className="grid grid-cols-2 gap-4 items-center">
                <button className="bg-secondary-color rounded-xl p-4  flex justify-center text-white gap-2 items-center">
                  <span className="font-medium">Cotizar viaje</span>
                </button>

                <button className="bg-primary-color rounded-xl p-4  flex justify-center text-white gap-2 items-center">
                  <span className="font-medium">Consultar reserva</span>
                </button>
              </div>
              <span>Escríbenos para un servicio más personalizado:</span>

              <button className="bg-whatsapp-color rounded-xl p-4  flex justify-center text-white gap-2 items-center">
                <FaWhatsapp size={24} />
                <span className="font-medium">Whatsapp</span>
              </button>
            </div>
            <div className="">
              <Image src={"colash-bus-trips.png"} alt="colash"></Image>
            </div>
          </div>
        </Section>
      </>
    );
  };

  const AboutUsSection = () => {
    return (
      <>
        {/* about us */}
        <Section id={"about_us_section"} className="flex flex-col">
          <div className="grid gap-4 grid-rows-2 md:grid-cols-2 md:grid-rows-1">
            <div className="grid gap-4">
              <div>
                <h6 className="text-green-500">
                  Transporte a cualquier parte del país
                </h6>
                <h3 className="text-3xl font-bold font-montserrat">Santo Domingo Bus</h3>
              </div>
              <p>
                Somos una empresa especializada en el transporte turístico y
                empresarial con más de 15 años de experiencia. Brindamos a
                nuestros clientes un servicio centrado en la calidad, la
                seguridad y la puntualidad.
              </p>
              <p>
                Nuestra filosofía empresarial se centra en la calidad y
                eficiencia en todos nuestros servicios, enfocados en nuestra
                visión, misión, y valores. Por ello creamos una estrecha
                realacion de confianza, estable y duradera con todos nuestros
                clientes.
              </p>
              <div className="flex items-center">
                <button className="bg-secondary-color rounded-xl p-4  flex justify-center text-white gap-2 items-center">
                  <span className="font-medium">Más información</span>
                  <BsChevronDoubleRight size={24} />
                </button>
              </div>
            </div>
            <div>
              <Image
                src={"AdobeStock_276219805.jpeg"}
                className="rounded-md"
                alt="AdobeStock_276219805"
              >
                Loading...
              </Image>
            </div>
          </div>
        </Section>
      </>
    );
  };

  const IntroSection = (props) => {
    return (
      <section className={`h-screen w-full relative ${props.className}`}>
        <div className="grid h-full md:grid-cols-2">
          <div></div>
          <div className="flex md:items-center px-14">
            <QuickSearchTripForm></QuickSearchTripForm>
          </div>
        </div>

        <Image
          src={"hn9mo4vDI0bUBLz68gqN.png"}
          className="-z-10 left-0 top-0 right-0 bottom-0"
          style={{ position: "absolute" }}
        ></Image>
      </section>
    );
  };

  const ContactUsSection = () => {
    return (
      <>
        {/* contact us */}
        <Section
          id={"contact_us_section"}
          className="flex flex-col gap-4 bg-slate-100"
        >
          <span className="block font-montserrat font-medium text-4xl mb-10">
            ¿Tienes alguna pregunta?
            <span className="font-bold block">
              Consulta y cotiza gratis con un agente
            </span>
          </span>
          <div className="grid grid-rows-2 md:grid-cols-2 md:grid-rows-1 gap-4">
            <div className="grid gap-4">
              <div className="bg-white p-4 rounded-xl">
                <label htmlFor="to" className="block text-xs font-medium">
                  Nombre<span className="text-red-500">*</span>
                </label>
                <input
                  type="text"
                  name="to"
                  className="w-full focus:border-none text-sm"
                  placeholder="Distrito Nacional, Sto. Dgo."
                />
              </div>

              <div className="bg-white p-4 rounded-xl">
                <label htmlFor="email" className="block text-xs font-medium">
                  E-mail<span className="text-red-500">*</span>
                </label>
                <input
                  type="email"
                  name="email"
                  className="w-full focus:border-none text-sm"
                  placeholder="e.g.: johndoe@mail.com"
                />
              </div>

              <div className="bg-white p-4 rounded-xl">
                <label htmlFor="company" className="block text-xs font-medium">
                  Compañía
                </label>
                <input
                  type="text"
                  name="company"
                  className="w-full focus:border-none text-sm"
                  placeholder="e.g.: Doe Company"
                />
              </div>

              <div className="bg-white p-4 rounded-xl">
                <label htmlFor="msg" className="block text-xs font-medium">
                  Mensaje<span className="text-red-500">*</span>
                </label>
                <textarea
                  type="text"
                  name="msg"
                  className="w-full focus:border-none text-sm"
                  placeholder="e.g.: johndoe@mail.com."
                ></textarea>
              </div>

              <div className="grid grid-cols-2 gap-2">
                <button className="bg-secondary-color rounded-xl p-4  flex justify-center text-white gap-2 items-center">
                  <span className="font-medium">Enviar solicitud</span>
                </button>
                <button className="bg-whatsapp-color rounded-xl p-4  flex justify-center text-white gap-2 items-center">
                  <FaWhatsapp size={24} />
                  <span className="font-medium">Whatsapp</span>
                </button>
              </div>
            </div>
          </div>
        </Section>
      </>
    );
  };

  const TestimonialsSection = () => {
    return (
      <>
        {/* testimonials */}
        <Section
          className="flex flex-col items-center"
          style={{ minHeight: "422px" }}
        >
          <div className="grid grid-cols-1 gap-6 md:gap-12 text-center md:py-12">
            <div className="flex flex-col">
              <h6 className="text-green-500 font-montserrat text-xs font-bold uppercase">Nuestros servicios fabrican</h6>
              <h2 className="text-4xl font-montserrat font-bold">Clientes felices</h2>
            </div>
            <div className="grid gap-4 grid-rows-3 md:grid-cols-3 md:grid-rows-1">
              {testimonials.map((testimonial) => (
                <TestimonialCard
                  key={testimonial.id}
                  testimony={testimonial}
                ></TestimonialCard>
              ))}
            </div>
          </div>
        </Section>
      </>
    );
  };

  const ServicesSection = () => {
    return (
      <>
        {/* services */}
        <Section
          className="flex flex-col items-center bg-primary-color-dark text-white"
          id={"services_section"}
          style={{ minHeight: "582px" }}
        >
          <div className="grid grid-cols-1 gap-8 text-center ">
            <div className="flex flex-col">
              <h6 className="text-green-500 font-montserrat text-xs font-bold uppercase">¿Cómo podemos ayudarte?</h6>
              <h2 className="text-4xl font-montserrat font-bold">
                Tenemos justo lo que necesitas
              </h2>
            </div>
            <div className="grid gap-4 grid-rows-3 md:grid-cols-3 md:grid-rows-1">
              {/* services goes here */}
              <ServiceCard>
                <ServiceCard.Image
                  image={"https://file.rendit.io/n/zarL2ElDC67kmoh6ScDB.png"}
                ></ServiceCard.Image>
                <ServiceCard.Title>Servicio de Transporte 01</ServiceCard.Title>
                <ServiceCard.Description>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor.
                </ServiceCard.Description>
              </ServiceCard>
              <ServiceCard>
                <ServiceCard.Image
                  image={"https://file.rendit.io/n/xuQb8SsOD4092HQQOnyE.png"}
                ></ServiceCard.Image>
                <ServiceCard.Title>Servicio de Transporte 02</ServiceCard.Title>
                <ServiceCard.Description>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor.
                </ServiceCard.Description>
              </ServiceCard>
              <ServiceCard>
                <ServiceCard.Image
                  image={"https://file.rendit.io/n/UXlLNq4D8Bw1YPc80LfF.png"}
                ></ServiceCard.Image>
                <ServiceCard.Title>Servicio de Transporte 03</ServiceCard.Title>
                <ServiceCard.Description>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor.
                </ServiceCard.Description>
              </ServiceCard>
            </div>
          </div>
        </Section>
      </>
    );
  };

  const ValuesSection = () => {
    return (
      <>
        {/* values */}
        <Section className="px-0 py-0 flex flex-col" style={{ padding: 0 }}>
          <div className="grid grid-rows-4 md:grid-cols-4 md:grid-rows-1 font-montserrat font-medium uppercase">
            <div className="h-56 flex gap-4 flex-col justify-center items-center">
              <BsShieldCheck size={48} />
              <span>Seguridad</span>
            </div>
            <div className="h-56 bg-slate-100 flex gap-4 flex-col justify-center items-center">
              <TbHeartHandshake size={48} />
              <span>Confianza</span>
            </div>
            <div className="h-56 bg-slate-200 flex gap-4 flex-col justify-center items-center">
              <BiCog size={48} />
              <span>Eficiencia</span>
            </div>
            <div className="h-56 bg-slate-300 flex gap-4 flex-col justify-center items-center">
              <FaBalanceScale size={48} />
              <span>Integridad</span>
            </div>
          </div>
        </Section>
      </>
    );
  };

  return (
    <Layout className={"transition-all ease-in-out"}>
      {/* intro */}
      <IntroSection></IntroSection>
      <CharacteristicsSection></CharacteristicsSection>
      <PlanYourNextTripSection></PlanYourNextTripSection>
      <AboutUsSection></AboutUsSection>
      <ServicesSection></ServicesSection>
      <ValuesSection></ValuesSection>
      <TestimonialsSection></TestimonialsSection>
      <ContactUsSection></ContactUsSection>
    </Layout>
  );
};

export default IndexPage;
