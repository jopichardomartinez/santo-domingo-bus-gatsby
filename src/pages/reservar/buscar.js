import React from "react";
import { Layout } from "../../components/layout";
import { VehicleSelectionCard } from "../../components/buscar/vehicle-selection-card";

export default function Buscar() {
  const vehicles = [
    {
      id: 1,
      name: "Vehículo 1",
      image:
        "https://images.unsplash.com/photo-1518791841217-8f162f1e1131?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
      vehicleType: {
        id: 1,
        name: "Luxury",
        price: 100,
      },
    },
    {
      id: 2,
      name: "Vehículo 2",
      image:
        "https://images.unsplash.com/photo-1518791841217-8f162f1e1131?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
      vehicleType: {
        id: 2,
        name: "Economy",
        price: 50,
      },
    },
    {
      id: 3,
      name: "Vehículo 3",
      image:
        "https://images.unsplash.com/photo-1518791841217-8f162f1e1131?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
      vehicleType: {
        id: 3,
        name: "Luxury",
        price: 100,
      },
    },
    {
      id: 4,
      name: "Vehículo 4",
      image:
        "https://images.unsplash.com/photo-1518791841217-8f162f1e1131?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
      vehicleType: {
        id: 4,
        name: "Economy",
        price: 50,
      },
    },
    {
      id: 5,
      name: "Vehículo 5",
      image:
        "https://images.unsplash.com/photo-1518791841217-8f162f1e1131?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
      vehicleType: {
        id: 5,
        name: "Luxury",
        price: 100,
      },
    },
  ];

  return (
    <Layout className="gap-4 flex-col flex ">
      <div className=" px-8 py-4">
        <h2 className="text-xl font-bold">Reservación</h2>

        <div className="grid grid-rows-6 md:grid-cols-6 gap-4 md:grid-rows-1">
          <div className="bg-white p-4 rounded-xl border-2">
            <label htmlFor="to" className="block text-sm font-medium">
              Hasta
            </label>
            <input
              type="text"
              name="to"
              className="w-full focus:border-none text-xl"
              placeholder="Bavaro, Punta Cana"
            />
          </div>

          <div className="bg-white p-4 rounded-xl border-2">
            <label htmlFor="to" className="block text-sm font-medium">
              Desde
            </label>
            <input
              type="text"
              name="to"
              className="w-full focus:border-none text-xl"
              placeholder="Distrito Nacional, Sto. Dgo."
            />
          </div>

          <div className="bg-white p-4 rounded-xl border-2">
            <label htmlFor="to" className="block text-sm font-medium">
              Fecha
            </label>
            <select name="date" className="w-full focus:border-none text-xl">
              <option className="text-gray-400" value={null}>
                DD/MM
              </option>
            </select>
          </div>

          <div className="bg-white p-4 rounded-xl border-2">
            <label htmlFor="to" className="block text-sm font-medium">
              Tipo de vehiculo
            </label>
            <select name="date" className="w-full focus:border-none text-xl">
              <option className="text-gray-400" value={null}>
                Luxury
              </option>
            </select>
          </div>

          <div className="bg-white p-4 rounded-xl border-2">
            <label htmlFor="to" className="block text-sm font-medium">
              No. pasajeros
            </label>
            <select name="date" className="w-full focus:border-none text-xl">
              <option className="text-gray-400" value={null}>
                2 pasajeros
              </option>
            </select>
          </div>

          <button className="bg-secondary-color rounded-xl p-4 flex justify-center text-white gap-2 items-center">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-5 w-5"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fillRule="evenodd"
                d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                clipRule="evenodd"
              />
            </svg>
            <span className="text-xl font-medium">Buscar</span>
          </button>
        </div>
      </div>
      <div className="bg-slate-100 flex flex-col p-8">
        <VehicleSelectionCard vehicle={vehicles[0]}></VehicleSelectionCard>
        <div className="">
          <h2 className="text-xl font-bold w-full border-b-4 pb-4 mb-8">
            Otras opciones disponibles
          </h2>
          <div className="flex gap-8 md:flex-row flex-col">
            {vehicles.slice(1, vehicles.length).map((vehicle) => (
              <VehicleSelectionCard
                vertical
                vehicle={vehicle}
              ></VehicleSelectionCard>
            ))}
          </div>
        </div>
      </div>
    </Layout>
  );
}
