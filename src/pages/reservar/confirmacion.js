import { navigate } from "gatsby";
import React from "react";
import { Layout } from "../../components/layout";

export default function Confirmacion() {

  return (
    <Layout className={"bg-slate-100"}>
      <div className="flex items-center justify-center h-full py-8">
        <div className="flex flex-col gap-8 p-24 text-center justify-center mx-8 bg-white">
          <div>
            <h2 className="text-3xl font-bold">¡Tu reserva está lista!</h2>
            <p className="text-gray-400">
              <span className="block">El proceso ha sido completado exitosamente</span>. Recibirás un correo de
              confirmación en los próximos minutos.
            </p>
          </div>

          <div className="flex justify-center">
            <button className="bg-secondary-color border-1 md:w-1/3 text-white py-2 px-4 rounded" onClick={() => navigate('/')}>
              Volver a inicio
            </button>
          </div>
        </div>
      </div>
    </Layout>
  );
}
