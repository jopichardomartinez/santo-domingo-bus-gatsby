import { navigate } from "gatsby";
import React, { useContext } from "react";
import { Layout } from "../../components/layout";
import { Stepper } from "../../components/steepper";
import {
  BookDataContext,
  BookDataProvider,
} from "../../contexts/book-data-context";

function StepRegistro() {
  return (
    <div className="flex flex-col gap-4">
      <h2 className="text-xl font-bold">Formulario de registro</h2>
      <div className="grid gap-4 grid-rows-2 md:grid-cols-2 md:grid-rows-1">
        <div>
          <label className="text-sm font-bold pb-2 block" htmlFor="name">Nombre</label>
          <input
            type="text"
            name="name"
            className="w-full focus:border-none text-xl bg-gray-100 py-2 px-4"
            placeholder="Nombre"
          />
        </div>
        <div>
          <label className="text-sm font-bold pb-2 block" htmlFor="dni">
            Cédula de identidad/Pasaporte
          </label>
          <input
            type="text"
            name="dni"
            className="w-full focus:border-none text-xl bg-gray-100 py-2 px-4"
            placeholder="Número de ID/Pasaporte"
          />
        </div>
      </div>
      <div className="grid gap-4 grid-rows-2 md:grid-cols-2 md:grid-rows-1">
        <div>
          <label className="text-sm font-bold pb-2 block" htmlFor="email">E-mail</label>
          <input
            type="text"
            name="email"
            className="w-full focus:border-none text-xl bg-gray-100 py-2 px-4"
            placeholder="example@mail.com"
          />
        </div>
        <div>
          <label className="text-sm font-bold pb-2 block" htmlFor="phone">Teléfono </label>
          <input
            type="text"
            name="phone"
            className="w-full focus:border-none text-xl bg-gray-100 py-2 px-4"
            placeholder="Número de teléfono"
          />
        </div>
      </div>

      <div className="flex flex-col md:grid-rows-1">
        <label className="text-sm font-bold pb-2 block" htmlFor="fromAddress">
          Dirección punto de salida
        </label>
        <input
          type="text"
          name="fromAddress"
          className="w-full focus:border-none text-xl bg-gray-100 py-2 px-4"
          placeholder="Dirección punto de salida"
        />
      </div>

      <div className="grid gap-4 grid-rows-2 md:grid-cols-6 md:grid-rows-1">
        <div>
          <label className="text-sm font-bold pb-2 block" htmlFor="date">Fecha</label>
          <select
            name="date"
            className="w-full focus:border-none text-xl bg-gray-100 py-2 px-4"
          >
            <option value="">dd/mm/aaaa</option>
          </select>
        </div>
        <div>
          <label className="text-sm font-bold pb-2 block" htmlFor="timeOfDay">Hora</label>
          <select
            name="timeOfDay"
            className="w-full focus:border-none text-xl bg-gray-100 py-2 px-4"
          >
            <option value="">hh:mm</option>
          </select>
        </div>
      </div>

      <div className="flex flex-col md:grid-rows-1">
        <label className="text-sm font-bold pb-2 block" htmlFor="toAddress">
          Dirección punto de destino
        </label>
        <input
          type="text"
          name="toAddress"
          className="w-full focus:border-none text-xl bg-gray-100 py-2 px-4"
          placeholder="Dirección de punto de destino"
        />
      </div>

      <div className="flex flex-col md:grid-rows-1">
        <label className="text-sm font-bold pb-2 block" htmlFor="comments">
          Comentarios adicionales
        </label>
        <textarea
          type="text"
          name="comments"
          className="w-full focus:border-none text-xl bg-gray-100 py-2 px-4"
          placeholder="Agregar comentario..."
          cols={16}
          maxLength={200}
        ></textarea>
      </div>
    </div>
  );
}

function StepPago() {
  return (
    <div className="flex flex-col gap-4">
      <h2 className="text-xl font-bold">Información de pago</h2>

      <div className={"flex"}>
        <div>
          <label htmlFor={"is-partial-payment"} className={"float-right pl-4"}>
            Pagar el 30% en línea
          </label>
          <input type={"checkbox"} name={"is-partial-payment"} />
        </div>
      </div>
      <div className={"flex"}>
        <div>
          <label htmlFor={"is-not-partial-payment"} className={"float-right pl-4"}>
            Pagar la totalidad del viaje en línea
          </label>
          <input type={"checkbox"} name={"is-not-partial-payment"} />
        </div>
      </div>
      <div className="flex gap-4 flex-col">
        <div className="flex flex-col md:grid-rows-1">
          <label className="text-sm font-bold pb-2 block" htmlFor="holder">
            Nombre del titular
          </label>
          <input
            type="text"
            name="holder"
            className="w-full focus:border-none text-xl bg-gray-100 py-2 px-4"
            placeholder="John Doe"
          />
        </div>
        <div className="flex flex-col md:grid-rows-1">
          <label className="text-sm font-bold pb-2 block" htmlFor="ccNumber">
            Número de tarjeta
          </label>
          <input
            type="text"
            name="ccNumber"
            className="w-full focus:border-none text-xl bg-gray-100 py-2 px-4"
            placeholder="0000 0000 0000 0000"
          />
        </div>

        <div className="grid gap-4 mb-8 grid-rows-2 md:grid-cols-2 md:grid-rows-1">
          <div>
            <label className="text-sm font-bold pb-2 block" htmlFor="expirationDate">
              Fecha de vencimiento
            </label>
            <input
              type="text"
              name="expirationDate"
              className="w-full focus:border-none text-xl bg-gray-100 py-2 px-4"
              placeholder="MM/AA"
            />
          </div>
          <div>
            <label className="text-sm font-bold pb-2 block" htmlFor="cvc">
              Código de seguridad
            </label>
            <input
              type="text"
              name="cvc"
              className="w-full focus:border-none text-xl bg-gray-100 py-2 px-4"
              placeholder="CVC"
            />
          </div>
        </div>
        <span className="block">Costo total de viaje USD 100.00</span>
        <div className="py-4">
          <h2 className="text-3xl font-bold">Total USD 100.00</h2>
          <span className="block">Impuestos incluidos</span>
        </div>
      </div>
    </div>
  );
}

function StepConfirmacion() {
  return (
    <div className="flex flex-col gap-4">
      <h2 className="text-xl font-bold">Confirmación</h2>
      <div className="flex flex-col gap-4">
        <StepRegistro></StepRegistro>
        <StepPago></StepPago>
      </div>
    </div>
  );
}

function Book() {
  const b = useContext(BookDataContext);

  const steps = [
    {
      title: "Reservar",
    },
    { title: "Pago" },
    { title: "Confirmar" },
  ];

  const handleNextPageChange = (page) => {
    if (b.page === 2) {
      navigate("/reservar/confirmacion");
      return;
    }

    if (page > steps.length) {
      return;
    }

    b.nextPage();
  };

  const handlePrevPageChange = (page) => {
    if (page < 0) {
      return;
    }

    b.prevPage();
  };

  return (
    <Layout className={"bg-slate-100"}>
      <div className="flex flex-col px-8 py-4 gap-4 items-stretch justify-center ">
        <div className="flex justify-center">
          <Stepper steps={steps} currentStep={b.page}></Stepper>
        </div>
        <div className="mt-4 flex gap-4 flex-col md:p-12 md:mx-24 bg-white rounded-xl p-8">
          <div className="flex-grow">
            {b.page === 0 && <StepRegistro></StepRegistro>}
            {b.page === 1 && <StepPago></StepPago>}
            {b.page === 2 && <StepConfirmacion></StepConfirmacion>}
          </div>
          <div className="flex flex-col gap-4 md:gap-0 md:pt-4 md:flex-row justify-between">
            <button
              className="border-secondary-color md:w-1/3 border-1 text-secondary-color py-2 px-4 rounded"
              onClick={() => handlePrevPageChange(b.page - 1)}
            >
              Anterior
            </button>
            <button
              className="bg-secondary-color border-1 md:w-1/3 text-white py-2 px-4 rounded"
              onClick={() => handleNextPageChange(b.page + 1)}
            >
              {b.page === 2 ? "Confirmar" : "Siguiente"}
            </button>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default function Reserva() {
  return (
    <BookDataProvider>
      <Book />
    </BookDataProvider>
  );
}
