import React from "react";
import { createContext, useState } from "react";

export const BookDataContext = createContext();

export const BookDataProvider = ({ children }) => {
  const [bookData, setBookData] = React.useState({
    user: {
      dni: "",
      fullName: "",
      email: "",
      phone: "",
      fromAddress: "",
      toAddress: "",
      date: "",
      timeOfTheDay: "",
      comments: "",
    },
    paymentData: {
      paymentMethod: "",
      isPartialPayment: false,
      cardData: {
        cardNumber: "",
        cardHolder: "",
        expirationMonth: "",
        expirationYear: "",
        cvv: "",
      },
      vehicle: {
        id: "",
        name: "",
        image: "",
        vehicleType: {
          id: "",
          name: "",
          price: "",
        },
      },
      passengersCount: "",
      from: {},
      to: {},
    },
  });
  const [page, setPage] = useState(0);

  const nextPage = () => {
    setPage(page + 1);
  };

  const prevPage = () => {
    setPage(page - 1);
  };

  const goTo = (page) => {
    setPage(page);
  };

  return (
    <BookDataContext.Provider
      value={{ bookData, setBookData, nextPage, prevPage, page, goTo }}
    >
      {children}
    </BookDataContext.Provider>
  );
};
