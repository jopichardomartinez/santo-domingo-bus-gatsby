import React from "react";
import { BsChevronDoubleRight } from "react-icons/bs";

export function ServiceCard({ children }) {
  return (
    <div className="flex flex-col justify-center justify-self-center items-center gap-4 max-w-xs">
      {children}
      <button className="bg-secondary-color rounded-md py-2 px-4  flex justify-center text-white gap-2 items-center">
        <span className="font-medium">Solicitar servicio</span>
        <BsChevronDoubleRight size={16} />
      </button>
    </div>
  );
}

ServiceCard.Image = ({ children, imageAlt, image }) => (
  <div className="flex justify-center">
    <div
      className="w-36 h-36 rounded-full p-2   border-white relative overflow-hidden"
      style={{ borderWidth: 0.5 }}
    >
      <img
        alt={imageAlt}
        src={image}
        className=" rounded-full h-full w-full object-cover"
      />
    </div>
  </div>
);

ServiceCard.Title = ({ children }) => (
  <h3 className="font-medium text-xs">{children}</h3>
);

ServiceCard.Description = ({ children }) => (
  <p className="text-xs font-light leading-4 text-center">{children}</p>
);
