import React from "react";
import { AiFillStar, AiOutlineStar } from "react-icons/ai";
import ReactStars from "react-rating-stars-component";

export function TestimonialCard({ testimony }) {
  return (
    <div className="grid grid-rows-2 gap-4">
      <p className="p-6 bg-slate-100 text-xs text-left after:content-[''] relative after:rotate-45 after:translate-y-2 after:left-8 after:w-4 after:h-4 after:absolute after:bg-slate-100 after:z-10 after:bottom-0 after:border-tl-">
        {testimony.comment}
      </p>
      <div className="flex gap-2 ml-4">
        <img
          className="w-10 h-10 rounded-full"
          alt="person"
          src={testimony.image}
        />
        <div className="flex flex-col gap-1 ml-2">
          <strong className="text-xs font-montserrat font-sans text-primary-color">{testimony.name}</strong>
          <ReactStars
            emptyIcon={<AiOutlineStar className="text-red-500" />}
            filledIcon={<AiFillStar className="text-red-500" />}
            size={16}
            edit={false}
            value={testimony.rating}
          />
        </div>
      </div>
    </div>
  );
}
