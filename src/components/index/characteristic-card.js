import React from "react";
import { Image } from "../image";

export function CharacteristicCard({
  characteristic,
  isImageALocalAsset = true,
}) {
  return (
    <div className="card flex flex-col gap-8 text-center">
      <div className="flex justify-center">
        {isImageALocalAsset ? (
          <Image
            className="w-24 h-24"
            src={characteristic.image}
            alt="cash icon"
          ></Image>
        ) : (
          <img src={characteristic.image} />
        )}
      </div>
      <h3 className="text-primary-color-dark py-8 font-bold">{characteristic.title}</h3>
      <p className="leading-4">{characteristic.description}</p>
    </div>
  );
}
