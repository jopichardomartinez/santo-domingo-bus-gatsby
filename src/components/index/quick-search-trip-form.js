import React from "react";
import { Image } from '../image';

export function QuickSearchTripForm() {
  function InputField({ children, ...rest }) {
    return (
      <div className="bg-white p-2 rounded-xl border-2">
        {children}
        <input
          className={`w-full focus:border-none text-xl ${rest.className}`}
          {...rest}
        />
      </div>
    )
  }

  InputField.Label = function ({ children, ...rest }) {
    return (
      <label
        {...rest}
        className={`block text-xs font-medium ${rest.className}`}
      >
        {children}
      </label>
    )
  }

  function SelectField({ children, ...rest }) {
    return (
      <div className="bg-white p-2 rounded-xl border-2">
        <SelectField.Label htmlFor={rest.name}>
          {rest.label}
        </SelectField.Label>
        <select {...rest} className={`w-full focus:border-none text-xl ${rest.className}`}>
          {children}
        </select>
      </div>
    );
  }

  SelectField.Option = function ({ children, ...rest }) {
    return (
      <option
        {...rest}
        className={`text-gray-400 ${rest.className}`}
      >
        {children}
      </option>
    )
  }

  SelectField.Label = function ({ children, ...rest }) {
    return (
      <label
        {...rest}
        className={`block text-xs font-medium ${rest.className}`}
      >
        {children}
      </label>
    )
  }

  return (
    <div className="flex flex-col w-full gap-4">
      <Image src={'intro_form_title_1_esp.svg'}></Image>
      <Image src={'intro_form_title_2_esp.svg'}></Image>
      <div className="grid grid-cols-2 gap-4">
        <InputField
          type="text"
          name="to"
          placeholder="Bavaro, Punta Cana">
          <InputField.Label htmlFor="to">
            Hasta
          </InputField.Label>
        </InputField>
        <InputField
          type="text"
          name="to"
          placeholder="Distrito Nacional, Sto. Dgo.">
          <InputField.Label htmlFor="to">
            Desde
          </InputField.Label>
        </InputField>
      </div>
      <div className="grid grid-cols-3 gap-4">
        <SelectField label="Fecha" name="date">
          <SelectField.Option value="">
            hh:mm
          </SelectField.Option>
        </SelectField>
        <SelectField label="Tipo de vehiculo" name="vehicleType">
          <SelectField.Option value="">
            Luxury
          </SelectField.Option>
        </SelectField>
        <SelectField label="No. pasajeros" name="passengersCount">
          <SelectField.Option value="">
            2 pasajeros
          </SelectField.Option>
        </SelectField>
      </div>
      <button className="bg-secondary-color rounded-xl p-4 flex justify-center text-white gap-2 items-center">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="h-5 w-5"
          viewBox="0 0 20 20"
          fill="currentColor"
        >
          <path
            fillRule="evenodd"
            d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
            clipRule="evenodd"
          />
        </svg>
        <span className="text-xl font-medium">Buscar</span>
      </button>
    </div>
  );
}
