import React from "react";
import { BsWhatsapp } from "react-icons/bs";

export function Footer() {
  function OurLocation() {
    return (
      <div className="mb-3 md:mb-0 py-3">
        <div className="flex md:justify-start justify-center  items-center mb-3">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5 fill-white"
            viewBox="0 0 20 20"
            fill="currentColor"
          >
            <path
              fillRule="evenodd"
              d="M5.05 4.05a7 7 0 119.9 9.9L10 18.9l-4.95-4.95a7 7 0 010-9.9zM10 11a2 2 0 100-4 2 2 0 000 4z"
              clipRule="evenodd"
            />
          </svg>
          <span className="text-white font-montserrat font-bold text-xl ml-1">
            Nuestra Locación
          </span>
        </div>
        <div className="md:text-left text-center">
          <span className="text-white text-sm">
            <span className="block">Calle 6ta con Calle B, Mi Hogar </span>
            Santo Domingo Este 11510, Rep. Dom{" "}
          </span>
        </div>
      </div>
    );
  }

  function OurSchedule() {
    return (
      <div className="mb-3 md:mb-0 py-3">
        <div className="flex items-center md:justify-start justify-center mb-3">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5 fill-white"
            viewBox="0 0 20 20"
            fill="currentColor"
          >
            <path
              fillRule="evenodd"
              d="M10 18a8 8 0 100-16 8 8 0 000 16zm1-12a1 1 0 10-2 0v4a1 1 0 00.293.707l2.828 2.829a1 1 0 101.415-1.415L11 9.586V6z"
              clipRule="evenodd"
            />
          </svg>
          <span className="text-white font-montserrat font-bold text-xl ml-1">Horario </span>
        </div>
        <div className="text-center md:text-left">
          <span className="text-white text-sm">
            <span className="block">Lunes a Viernes </span>
            8:00 a 05:00 p.m.{" "}
          </span>
        </div>
      </div>
    );
  }

  function ContactUs() {
    return (
      <div className="flex mb-3 flex-col md:flex-row text-center gap-4 md:gap-0 md:mb-0 py-3 items-center">
        <div className="flex items-center font-montserrat">
          <span className="text-white text-sm ml-1">
            <span className="block">Escribenos para un </span>{" "}
            <strong>servicio más personalizado </strong>
          </span>
        </div>
        <div>
          <button className="bg-whatsapp-color rounded md:ml-11 px-9 py-2 text-xl text-white flex items-center gap-2">
            <BsWhatsapp></BsWhatsapp>
            Whatsapp
          </button>
        </div>
      </div>
    );
  }

  function FooterDivider() {
    return (
      <div className="items-center hidden md:flex  justify-center px-12">
        <div className="h-full w-0.5 bg-red-500"></div>
      </div>
    );
  }

  return (
    <footer className="bg-primary-color md:flex flex-col md:items-center md:justify-between">
      <div className="flex md:flex-col items-center justify-center w-full md:h-44">
        <div className="flex flex-col md:flex-row">
          <OurLocation></OurLocation>
          <FooterDivider></FooterDivider>
          <OurSchedule></OurSchedule>
          <FooterDivider></FooterDivider>
          <ContactUs></ContactUs>
        </div>
      </div>
      <div className="h-16 w-full bg-primary-color-dark flex items-center justify-center text-white">
        Copyright ©{new Date().getFullYear()}. Todos los derechos reservados
      </div>
    </footer>
  );
}
