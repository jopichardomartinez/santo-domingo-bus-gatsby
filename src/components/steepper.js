import React from "react";
import PropTypes from "prop-types";
import { BsCheck } from "react-icons/bs";

export function Stepper({ steps, currentStep }) {
  return (
    <div className="flex relative  ">
      {steps.map((step, index) => (
        <Step
          index={index}
          active={currentStep === index}
          complete={currentStep > index}
          title={step.title}
        ></Step>
      ))}
    </div>
  );
}

function Step({ title, onClick, active, complete, index, isLastStep }) {
  return (
    <div
      className={`flex px-4 flex-col relative items-center cursor-pointer ${
        !active && !complete && "opacity-40"
      }`}
      //   onClick={onClick}
    >
      <div
        className={`flex justify-center items-center rounded-full text-black w-8 h-8 ${
          active && "border-green-500 border-2 text-green-500 font-bold"
        } ${complete && "bg-green-500 text-white"} ${
          !active && !complete && "bg-gray-200"
        }`}
      >
        {complete ? <BsCheck color="#fff" size={24}></BsCheck> : index + 1}
      </div>
      <div className="step-title text-center">{title}</div>
    </div>
  );
}

Step.propTypes = {
  title: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  active: PropTypes.bool.isRequired,
};

Stepper.propTypes = {
  steps: PropTypes.arrayOf(PropTypes.object),
  currentStep: PropTypes.number,
};
