import React from "react";
import styled from "styled-components";
import {Footer} from "./footer";
import Navbar from "./navbar";

export const LayoutWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Main = styled.main`
  flex-grow: 1;
  overflow: hidden;
`;

export const Layout = ({children, className}) => (<LayoutWrapper className="min-h-screen ">
        <Navbar></Navbar>
        <Main className={`${className}`}>{children}</Main>
        <Footer></Footer>
    </LayoutWrapper>);
