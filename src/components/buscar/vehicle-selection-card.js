import { navigate } from "gatsby";
import React from "react";
import { BsCircleFill } from "react-icons/bs";

export function VehicleSelectionCard({ vehicle, vertical = false }) {
  return (
    <div
      className={`grid mb-8 ${
        vertical ? "grid-rows-2" : "grid-rows-2 md:grid-rows-1 md:grid-cols-2"
      } bg-white p-8`}
    >
      <div className="flex-grow flex justify-center ">
        <img src={"https://file.rendit.io/n/UzBKadTsN6KErrxPRrDU.png"} alt="" />
      </div>
      <div className={"flex flex-col justify-end flex-grow items-center px-4"}>
        <div className="flex flex-col text-center">
          <h2 className={"text-3xl font-bold text-primary-color"}>
            USD${Math.ceil(Math.random() * 100)}.00
          </h2>
          <span
            className={
              "flex text-sm items-center justify-center gap-2 text-gray-400"
            }
          >
            {vehicle.vehicleType.name} <BsCircleFill size={4} />{" "}
            {vehicle.vehicleType.capacity} p/max
          </span>
          <div className={"py-4"}>
            <label htmlFor={"is-roadTrip"} className={"float-right pl-4"}>
              Viaje de ida y vuelta
            </label>
            <input type={"checkbox"} name={"is-roadTrip"} />
          </div>
        </div>

        <button className="bg-whatsapp-color rounded-xl p-4 w-1/2  flex justify-center text-white gap-2 items-center" onClick={() => navigate('/reservar/reserva')}>
          <span className="font-medium">Reservar</span>
        </button>
      </div>
    </div>
  );
}
