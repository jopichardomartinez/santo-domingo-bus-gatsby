import { useEffect, useState } from "react";
import { useAxios } from "./use-axios";

export function useVehicleType() {
  const { axios } = useAxios();
  const [vehicleTypes, setVehicleTypes] = useState([]);

  async function findAll() {
    return await axios.get(`${process.env.REACT_APP_API_URL}/vehicle-type`);
  }

  useEffect(() => {
    if (vehicleTypes.length < 1) {
      findAll().then((vehicleTypes) => {
        setVehicleTypes(vehicleTypes.data);
      });
    }
  }, [vehicleTypes]);

  return {
    findAll,
    vehicleTypes,
  };
}
