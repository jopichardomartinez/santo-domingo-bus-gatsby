import axios from 'axios';

export function useAxios() {
    return {
        axios
    }
}