import './src/fonts/Montserrat-Regular.ttf';
import './src/fonts/Montserrat-SemiBold.ttf';
import './src/fonts/Montserrat-Bold.ttf';
import './src/fonts/Montserrat-ExtraBold.ttf';
import './src/fonts/Montserrat-ExtraLight.ttf';
import './src/fonts/Montserrat-Light.ttf';
import './src/fonts/Montserrat-Medium.ttf';
import './src/fonts/Montserrat-Thin.ttf';
import './src/fonts/Montserrat-ExtraLight.ttf';
import './src/fonts/Montserrat-Light.ttf';

import './src/styles/global.css'
